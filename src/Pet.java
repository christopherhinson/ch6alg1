public class Pet {
    private String name;
    private String animal;
    private int age;

    public Pet(String name, String animal, int age)
    {
        this.name = name;
        this.animal = animal;
        this.age = age;
    }

    public String getname ()
    {
        return name;
    }

    public String getAnimal()
    {
        return animal;
    }

    public String getName()
    {
        return name;
    }
}
